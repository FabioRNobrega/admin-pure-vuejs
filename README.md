# Admin POC Only with Vue.js | Lemoney

This project was generated with [Vue CLI](https://cli.vuejs.org/guide/)
We use:
 - Vue CLI: 4.5.11
 - VueJs: 2.6.12
 - Node: 14.15.0
 - NPM: 6.14.8
 - OS: linux x64

Table of contents
=================

  * [Install](#install)
  * [Usage](#usage)
  * [Tests](#tests)
  * [Important Libraries](#important-libraries)
  * [Architecture overview](#architecture-overview)

## Install 

+ Clone the repo and cd into it

``` bash
$ npm install
```

## Usage 

```bash 
$ npm run dev 
```

The application will become available at the URL:

```
http://localhost:8080/
```

## Tests

This project uses the Jest for test, that you can find on the tests folder. And run by:

```
npm run test

```
## Important Libraries

+ [Vue Bootstrap 2.21.2](https://bootstrap-vue.org/docs)
+ [Bootstrap 4.6](https://getbootstrap.com/docs/4.0/getting-started/introduction/)
+ [Vue Font Awesome 2.0.2](https://github.com/FortAwesome/vue-fontawesome)
+ [Axios](https://github.com/axios/axios)
+ [Vue Router](https://router.vuejs.org/installation.html)
## Architecture overview
This is a simple site for one POC.
