import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('../pages/index.vue'),
  },
  {
    path: '/advertiser',
    name: 'Advertisers',
    component: () => import('../pages/advertiser/index.vue'),
  },
];

const router = new VueRouter({
  routes,
});

export default router;
