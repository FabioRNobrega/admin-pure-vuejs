import { shallowMount } from '@vue/test-utils';
import Dashboard from '@/pages/index.vue';

describe('Dashboard', () => {
  let wrapper;

  const stubs = {
    'font-awesome-icon': true,
  };

  describe('#render', () => {
    it('renders correctly', () => {
      wrapper = shallowMount(Dashboard, {
        stubs,
      });
      expect(wrapper.element).toMatchSnapshot();
    });
  });
});
