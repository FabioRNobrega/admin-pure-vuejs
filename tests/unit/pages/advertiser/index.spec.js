import { shallowMount } from '@vue/test-utils';
import AdvertiserList from '@/pages/advertiser/index.vue';

const stubs = {
  'b-pagination': true,
};

const mockedAxios = { get: jest.fn() };
const mockedFetchData = jest.fn();

describe('Advertiser List', () => {
  let wrapper;

  describe('#render', () => {
    it('renders correctly', () => {
      wrapper = shallowMount(AdvertiserList, {
        stubs,
        mocks: {
          $axios: mockedAxios,
        },
      });
      expect(wrapper.element).toMatchSnapshot();
    });
  });

  describe('hooks', () => {
    describe('#created', () => {
      it('calls #fetchData', () => {
        wrapper = shallowMount(AdvertiserList, {
          stubs,
          mocks: {
            $axios: mockedAxios,
          },
          methods: {
            fetchData: mockedFetchData,
          },
        });
        expect(mockedFetchData).toHaveBeenCalled();
      });
    });
  });
});
