import { shallowMount } from '@vue/test-utils';
import NavBar from '@/components/nav-bar.vue';

describe('Dashboard', () => {
  let wrapper;

  const stubs = {
    'router-link': true,
  };

  describe('#render', () => {
    it('renders correctly', () => {
      wrapper = shallowMount(NavBar, {
        stubs,
      });
      expect(wrapper.element).toMatchSnapshot();
    });
  });
});
